```
		██╗    ██╗██████╗  ██████╗  ██████╗ ██╗   ██╗███████╗
		██║    ██║██╔══██╗██╔═══██╗██╔════╝ ██║   ██║██╔════╝
		██║ █╗ ██║██████╔╝██║   ██║██║  ███╗██║   ██║█████╗  
		██║███╗██║██╔══██╗██║   ██║██║   ██║██║   ██║██╔══╝  
		╚███╔███╔╝██║  ██║╚██████╔╝╚██████╔╝╚██████╔╝███████╗
		 ╚══╝╚══╝ ╚═╝  ╚═╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚══════╝
```

This is Wrogue Rewrite, an attempt to fix the problems in Wrogue which made it unworkable. Namely, this includes replacing Ncurses with BearLibTerminal, and having some sort of architecture planning to keep my spaghetti straight. 

The resources I'm using to build this game are:

- http://cplusplus.com, as I am still a novice;

- https://gameprogrammingpatterns.com, which is an absolutely amazing reference on simple, effective code structures;

- https://redblobgames.com, which is a similarly fantastic guide through some more advanced game topics. I'm using it as a guide for enemy pathing, FOV, and a tool to tune numbers with.

- The brains of some good friends

In Wrogue, you play a dashing young @, delving depths to recover the Amulet of Odlarbej. It is decidedly silly, or at least going to be. 
