#ifndef MOVE_H
#define MOVE_H
#include <vector>

struct Direction{
	int dx;
	int dy;
	Direction(int dx, int dy)
	:dx(dx),
	dy(dy)
	{}
};
const Direction NORTH(0, -1);
const Direction NORTHEAST(1, -1);
const Direction EAST(1, 0);
const Direction SOUTHEAST(1, 1);
const Direction SOUTH(0, 1);
const Direction SOUTHWEST(-1, 1);
const Direction WEST(-1, 0);
const Direction NORTHWEST(-1, -1);
std::vector<Direction> Directions = {NORTH, NORTHEAST, EAST, SOUTHEAST, SOUTH, SOUTHWEST, WEST, NORTHWEST};
#endif