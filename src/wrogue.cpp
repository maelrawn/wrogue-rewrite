#include "menu.hpp"
#include "movement.hpp"
#include "map.hpp"
#include "entity.hpp"
#include "func.hpp"
//#include "command.hpp"
//#include "movement.hpp"
//#include "dungeon.hpp"
//#include "render.hpp"

int main(){
	srand(time(NULL));
	initBearLib();
	while(1){
	int gameAction = displayMainMenu();
		switch(gameAction){
			case 0 /*New Game*/:{
				Map map;
				map.generateSpaces();
				Player player;
				char input = ' ';
				while(input != TK_ESCAPE){
					drawElements(map);
					drawPlayer(player);
					terminal_refresh();
					input = terminal_read();
					switch(input){
						case TK_KP_8:
							move(map, player, NORTH);
						break;
						case TK_KP_9:
							move(map, player, NORTHEAST);
						break;
						case TK_KP_6:
							move(map, player, EAST);
						break;
						case TK_KP_3:
							move(map, player, SOUTHEAST);
						break;
						case TK_KP_2:
							move(map, player, SOUTH);
						break;
						case TK_KP_1:
							move(map, player, SOUTHWEST);
						break;
						case TK_KP_4:
							move(map, player, WEST);
						break;
						case TK_KP_7:
							move(map, player, NORTHWEST);
						break;
					}
				}
			break;
			}
			case 1 /*Load Game*/:
			break;
			case 2 /*Story*/:
				displayStory();
			break;
			case 3 /*Options*/:
			break;
			case 4 /*Quit*/:
				terminal_close();
				exit(2);
			break;
		}
	}
}