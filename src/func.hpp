//Moving general functions into their own file avoids dependency conflicts
//and lets me make generally more complicated functions
//Thank you, stackexchange

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

//Movement Functions:

bool isInBounds(int xpos, int ypos){
	if(xpos >= 0 && xpos < 80 && ypos >= 0 && ypos < 24)
		return true;
	else
		return false;
}
bool move(Map& map, Entity& entity, const Direction direction){
	if(isInBounds(entity.getXPosition() + direction.dx, entity.getYPosition() + direction.dy)){
		if(!map.getTile(entity.getXPosition() + direction.dx, entity.getYPosition() + direction.dy).getCollision()){
			entity.setXPosition(entity.getXPosition() + direction.dx);
			entity.setYPosition(entity.getYPosition() + direction.dy);		
			return true;
		}
	return false;
	}
}
void drawElements(Map& map){
	terminal_clear();
	for(int i = 0; i < 80; i++){
		for(int j = 0; j < 24; j++){
			Tile& tile = map.getTile(i, j);
			tile.activateColor();
			terminal_print(i, j, tile.getGraphic());
		}
	}
}

void drawPlayer(Player player){
	player.activateColor();
	terminal_print(player.getXPosition(), player.getYPosition(), player.getGraphic());
}
//Combat Functions:


#endif