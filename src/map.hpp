#ifndef MAP_H
#define MAP_H
#include "tile.hpp"

class Map{
private:
	Tile tilearray[80][24];
	/*std::vector<Tile> accessibleTiles;
	std::vector<Tile> openTiles;
	std::vector<Tile> wallTiles;*/
	int mapXmax = 80 /*- uiwidth*/;
	int mapYmax = 24;
	int detailPasses;
	int startx;
	int starty;
	int endx;
	int endy;

public:
	Map(){generateSpaces();}
	int getXMax(){
		return mapXmax;
	}
	int getYMax(){
		return mapYmax;
	}
	Tile& getTile(int xpos, int ypos){
		return tilearray[xpos][ypos];
	}
	bool getCollision(int xpos, int ypos){
		return tilearray[xpos][ypos].getCollision();
	}
	virtual void generateSpaces(){
		for(int i = 0; i < 80; i++){
			for(int j = 0; j < 24; j++){
				this->tilearray[i][j] = Tile(chooseTileType(), i, j);
			}
		}
	}
	virtual void generateDetail(int passes){};
	virtual void ensureSpawnPoint(){};

	//This function can come later, need proof of concept for tiletype first
	/*Map(int startx, int starty)
	:startx(startx),
	starty(starty)
	{
		generateSpaces();
		generateDetail(this->detailPasses);
		ensureSpawnPoint();
		for(int i = 0; i < mapXmax; i++){
			for(int j = 0; j < mapYmax; j++){
				Tile& tile = tilearray[i][j];
				for(int k = 0; k < Directions.size(); k++){
					int newx = tile.getXPos() + Directions.at(k).dx;
					int newy = tile.getYPos() + Directions.at(k).dy;
					if(isInBounds(newx, newy)){
						if(!this->tilearray[newx][newy].getCollision()){
							tile.getNeighborTiles().push_back(this->tilearray[newx][newy]);
						}
					}
				}
			}
		}
		findUsableSpace(startx, starty);
	}*/

	/*void findUsableSpace(int startx, int starty){
		for(int i = 0; i < Directions.size(); i++){
			if(isInBounds(startx + Directions.at(i).dx, starty + Directions.at(i).dy)){
				Tile& tile = tilearray[startx][starty];
				int newx = tile.getXPos() + Directions.at(i).dx;
				int newy = tile.getYPos() + Directions.at(i).dy;
				if(isInBounds(newx, newy)){
					if(!this->tilearray[newx][newy].getCollision() 
					&& std::find(accessibleTiles.begin(), accessibleTiles.end(), Tile(nullptr, newx, newy)) == accessibleTiles.end()){
						accessibleTiles.push_back(this->tilearray[newx][newy]);
						findUsableSpace(newx, newy);
					}
				}
			}
		}
	}*/
};

class CaveMap: protected Map{

};

class DungeonMap: protected Map{

};

class ForestMap: protected Map{

};

class VolcanicMap: protected Map{

};

class LairOfOdlarbej: protected Map{

};
#endif