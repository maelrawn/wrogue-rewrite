#ifndef ENTITY_H
#define ENTITY_H
//This is Wrogue's implementation of the "typeobject"
//pattern from Game Programming Patterns.
//Also used in Wrogue is the "command" pattern.
class Entity{
private:
	int xposition;
	int yposition;
public:
	virtual int getXPosition(){
		return xposition;
	}
	virtual void setXPosition(int newxposition){
		xposition = newxposition;
	}
	virtual int getYPosition(){
		return yposition;
	}
	virtual void setYPosition(int newyposition){
		yposition = newyposition;
	}
};

class Player: public Entity{
private:
	int xpos = 40;
	int ypos = 12;
	const char* graphic = "@";
	Color color = Yellow;

public:
	Player(){}
	int getXPosition(){
		return xpos;
	}
	void setXPosition(int newxpos){
		xpos = newxpos;
	}
	int getYPosition(){
		return ypos;
	}
	void setYPosition(int newypos){
		ypos = newypos;
	}
	void activateColor(){
		this->color.activate();
	}
	const char* getGraphic(){
		return graphic;
	}
};

/*class Creature: protected Entity{
friend class CreatureType;
private:
	CreatureType* type;
	const char* name = NULL;
	int health = NULL;
	int xp = NULL;
	BehaviorEnum behavior = NULL;
public:
	Creature(CreatureType type)
	:creature.setStats(type.getStats)
	{}

};

enum class BehaviorEnum{
	no_behavior,
	melee_attacker,
	ranged_attacker,
	magic_attacker,
	melee_and_ranged_attacker
};

class CreatureType{
private:
	Creature* newCreature() = new Creature(*this);

};*/
#endif