#ifndef MENU_H
#define MENU_H
#include <BearLibTerminal.h>
#include <string>
int screenXmax = 80;
int screenYmax = 25;

void initBearLib(){
	terminal_open();
	terminal_set("window.title=Wrogue;");
	terminal_color(color_from_name("white"));
	terminal_color(color_from_name("black"));
	//terminal_composition(true);

}

void displayTitle(){
	const char* titlelines[] = {
		"██╗    ██╗██████╗  ██████╗  ██████╗ ██╗   ██╗███████╗",
		"██║    ██║██╔══██╗██╔═══██╗██╔════╝ ██║   ██║██╔════╝",
		"██║ █╗ ██║██████╔╝██║   ██║██║  ███╗██║   ██║█████╗  ",
		"██║███╗██║██╔══██╗██║   ██║██║   ██║██║   ██║██╔══╝  ",
		"╚███╔███╔╝██║  ██║╚██████╔╝╚██████╔╝╚██████╔╝███████╗",
		" ╚══╝╚══╝ ╚═╝  ╚═╝ ╚═════╝  ╚═════╝  ╚═════╝ ╚══════╝"
	};
	terminal_color(color_from_name("cyan"));
	for (int i = 0; i < 6; i++){
		terminal_print(screenXmax/2 - 26, screenYmax/4 + i, titlelines[i]);
	}
	terminal_refresh();
}

void displayMenuOptions(){
	const char* options[] = {
		"New Game",
		"Load Game",
		"Story",
		"Options",
		"Quit"
	};
	terminal_color(color_from_name("white"));
	for (int i = 0; i < 5; i++){
		terminal_print(screenXmax/2 - 4, screenYmax/2 + i, options[i]);
	}
	terminal_refresh();
}

int navigateMenu(){
	int selection = 0;
	char input = ' ';
	while(input != TK_RETURN){
		terminal_clear_area(screenXmax/2 - 6, screenYmax/2, 1, 6);
		terminal_print(screenXmax/2 - 6, screenYmax/2 + selection, ">");
		terminal_refresh();
		input = terminal_read();
		switch(input){
			case TK_UP:
			case TK_KP_8:
				if(selection - 1 >= 0)
					selection--;
			break;
			case TK_DOWN:
			case TK_KP_2:
				if(selection + 1 < 5)
					selection++;
			break;
		}
	}
	return selection;
}

int displayMainMenu(){
	terminal_clear_area(0, 0, screenXmax, screenYmax);
	displayTitle();
	displayMenuOptions();
	return navigateMenu();
}

void displayStory(){
	const char* storyBig[] = {
		"███████╗████████╗ ██████╗ ██████╗ ██╗   ██╗",
		"██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗╚██╗ ██╔╝",
		"███████╗   ██║   ██║   ██║██████╔╝ ╚████╔╝ ",
		"╚════██║   ██║   ██║   ██║██╔══██╗  ╚██╔╝  ",
		"███████║   ██║   ╚██████╔╝██║  ██║   ██║   ",
		"╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝   ╚═╝   "
	};
	const char* theStory[] = {
		"It is the end of an era; one of mythical happenings...",

		"The highest evil the land has seen was defeated at the",
		"Battle of Dreum Tower in a final, desperate push by ",
		"the failing forces of man.",

		"The dispatcher of Odlarbej, leader of the legions of undead,",
		"slew him in a single blow: an attack so fierce, the exquisite",
		"steel of Odlarbej's blade shattered and his head was cleaved",
		"from his body. The battle was won some time thereafter.",

		"His amulet was thought mundane and lost in the ongoing chaos.",
		"However, it has been revealed that it was in fact a powerful charm",
		"and must be recovered before it can bring dark to the land again.",
		"As you come into its resting place, you feel a sense of dread..."
	};
	terminal_clear_area(0, 0, screenXmax, screenYmax);
	terminal_color(color_from_name("cyan"));
	int centerScreen = screenXmax/2;
	for(int i = 0; i < 6; i++){
		terminal_print(centerScreen - 21, screenYmax/10+i, storyBig[i]);
	}
	terminal_color(color_from_name("white"));
	terminal_print(centerScreen - 28, screenYmax/4 + 3, theStory[0]);
	for(int i = 1; i < 4; i++){
		terminal_print(centerScreen - 28, screenYmax/3 + i + 2, theStory[i]);
	}
	for(int i = 1; i < 5; ++i){
		terminal_print(centerScreen - 31, screenYmax/3 + 6 + i, theStory[3+i]);
	}
	for(int i = 1; i < 5; ++i){
		terminal_print(centerScreen - 33, screenYmax/3 + 11 + i, theStory[7+i]);
	}
	terminal_refresh();
	terminal_read();
}
#endif