#ifndef TILE_H
#define TILE_H
#include <cstdlib>
#include "color.hpp"
#include <vector>
#include "item.hpp"

class TileType{
private:
	Color color;
	const char* graphic;
	bool collision;
	bool transparency;
public:
	TileType(Color color, const char* graphic, bool collision, bool transparency)
	:color(color),
	graphic(graphic),
	collision(collision),
	transparency(transparency)
	{}
	Color getColor(){
		return color;
	}
	const char* getGraphic(){
		return graphic;
	}
	bool hasCollision(){
		return collision;
	}
	bool hasTransparency(){
		return transparency;
	}
};
TileType *grassType = new TileType(Green, ",", false, true); 
TileType *wallType = new TileType(Flame, "#", true, false); 
TileType *defaultType = new TileType(Yellow, " ", false, true);

class Tile{
private:
	//std::vector<Item> heldItems = {};
	std::vector<Tile> neighborTiles = {};
	TileType* type;
	int xpos = NULL;
	int ypos = NULL;
public:
	
	Tile()
	:type(defaultType)
	{}

	Tile(TileType* type, int xpos, int ypos)
	:type(type),
	xpos(xpos),
	ypos(ypos)
	{}
	bool operator==(const Tile& other){
		bool isSimilar = true;
		if(other.xpos != this->xpos)
			isSimilar = false;
		if(other.ypos != this->ypos)
			isSimilar = false;
		return isSimilar;
	}
	void operator=(Tile* other){
		other->neighborTiles = this->neighborTiles;
		other->type = this->type;
		other->xpos = this->xpos;
		other->ypos = this->ypos;
	}
	std::vector<Tile> getNeighborTiles(){
		return neighborTiles;
	}
	void activateColor(){
		this->type->getColor().activate();
	}
	bool getCollision(){
		return type->hasCollision();
	}
	const char* getGraphic(){
		return this->type->getGraphic();
	}
	void setType(TileType* newtype){
		this->type = newtype;
	}
	int getXPos(){
		return xpos;
	}
	int getYPos(){
		return ypos;
	}
};

TileType* chooseTileType(){
	int num = rand() % 2;
	switch(num){
		case 0:
			return grassType;
		break;
		case 1:
			return wallType;
		break;
	}
}
#endif