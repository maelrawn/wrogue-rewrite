#ifndef FLOOR_H
#define FLOOR_H
#include "map.hpp"
#include "loot.hpp"

class Floor{
private:
	int depth;
	const char* name;
	Map& floormap;
	lootTable table;
public:
	Map* getMap(){
		return floormap;
	}
	const char* getName(){
		return name;
	}

};
#endif