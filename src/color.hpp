/**By name: 
grey (or gray),
red,
flame,
orange,
amber,
yellow,
lime,
chartreuse,
green,
sea,
turquoise,
cyan,
sky,
azure,
blue,
han,
violet,
purple,
fuchsia,
magenta,
pink,
crimson,
transparent.
*/
#ifndef COLOR_H
#define COLOR_H
#include <BearLibTerminal.h>
class Color{
private:
	const char* foregroundColor = NULL;
	const char* backgroundColor = NULL;
public:
	Color(const char* foregroundColor, const char* backgroundColor)
	:foregroundColor(foregroundColor),
	backgroundColor(backgroundColor)
	{}
	void activate(){
		terminal_color(color_from_name(this->foregroundColor));
		terminal_bkcolor(color_from_name(this->backgroundColor));
	}
}Green("green", "dark green"), Flame("dark grey", "grey"), Yellow("light yellow", "transparent");
#endif